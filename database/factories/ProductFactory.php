<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(1),
        'price' => $faker->numberBetween($min = 10000, $max = 10000000),
        'discount' => $faker->numberBetween($min = 0, $max = 30)
    ];
});
