<?php

use Illuminate\Database\Seeder;
use App\Material;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Material::class, 20)->create();
    }
}
