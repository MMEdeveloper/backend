<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = 
    [
        'supplier_id', 'comment'
    ];    

    public function orderedItem()
    {
        return $this->hasMany(OrderedItem::class);
    }
}