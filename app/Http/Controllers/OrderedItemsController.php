<?php

namespace App\Http\Controllers;

use App\OrderedItem;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;

class OrderedItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return OrderedItem::latest()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $order = Order::create([ 
            'supplier_id' => $request->supplier_id,
            'comment' => $request->comment
        ]);
            
        foreach( $request['items'] as $item ) {
            
                    $orderedItem = new OrderedItem;
                    $orderedItem->order_id = $order->id;
                    $orderedItem->itemable_id = $item['id'];
                    $orderedItem->itemable_type = $item['type'];
                    $orderedItem->amount = $item['amount'];
                    $orderedItem->save();
                
        }       
                return $orderedItem;


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderedItem  $orderedItem
     * @return \Illuminate\Http\Response
     */
    public function show(OrderedItem $orderedItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderedItem  $orderedItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderedItem $orderedItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderedItem  $orderedItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderedItem $orderedItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderedItem  $orderedItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderedItem $orderedItem)
    {
        //
    }
}
